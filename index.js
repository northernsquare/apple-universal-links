
var express = require('express');
var server = express();
const PORT = process.env.PORT || 80

// This will be call by APPLE TO VERIFY THE APP-SITE-ASSOCIATION 
// Make the 'apple-app-site-association' accessable to apple to verify the association
server.get('/.well-known/apple-app-site-association', function(request, response) {
    response.type('application/json')
    response.sendFile(__dirname +  '/apple-app-site-association');
});

// HOME PAGE
server.get('/', function(request, response) {
  response.sendFile(__dirname +  '/index.html');
});

// HOME PAGE
server.get('/handla', function(request, response) {
  response.sendFile(__dirname +  '/handla.html');
});

// ABOUT PAGE
server.get('/recept', function(request, response) {
  response.sendFile(__dirname +  '/recept.html');
});

server.get('/favicon.ico', function(request, response) {
  response.sendFile(__dirname +  '/favicon.ico');
});

server.listen(PORT, () =>
    console.log(`Listening on ${ PORT }`)
);